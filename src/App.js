import logo from "./logo.svg";
import "./App.css";
import HookPhoneCart from "./HookPhoneCart/HookPhoneCart";

function App() {
  return (
    <div className="App">
      <HookPhoneCart />
    </div>
  );
}

export default App;
