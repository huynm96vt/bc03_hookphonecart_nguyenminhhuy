import React, { memo } from "react";

function CardItem({
  hinhAnh,
  tenSP,
  handleShowDetail,
  product,
  setProduct,
  showModal,
  themGioHang,
}) {
  return (
    <div className="col-4">
      <div className="card" style={{ width: "18rem" }}>
        <img
          style={{ width: "100%", height: "300px", objectFit: "cover" }}
          src={hinhAnh}
          alt="Card image cap"
        />
        <div className="card-body">
          <h5 className="card-title">{tenSP}</h5>

          <a
            onClick={(e) => {
              handleShowDetail();
              e.preventDefault();
              setProduct(product);
            }}
            href="#"
            className="btn btn-success"
          >
            Xem chi tiet
          </a>
          <a
            onClick={(e) => {
              showModal();
              themGioHang(product);
              e.preventDefault();
            }}
            href="#"
            className="btn btn-danger"
          >
            Them gio hang
          </a>
        </div>
      </div>
    </div>
  );
}

export default memo(CardItem);
