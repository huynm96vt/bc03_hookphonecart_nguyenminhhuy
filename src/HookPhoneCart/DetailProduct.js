import React, { memo } from "react";

function DetailProduct({
  manHinh,
  heDieuHanh,
  cameraTruoc,
  cameraSau,
  ram,
  rom,
  giaBan,
  tenSP,
  hinhAnh,
}) {
  return (
    <div className="row pt-5">
      <div className="col-4 text-center">
        <h1>{tenSP}</h1>
        <img style={{ width: "100%" }} src={hinhAnh} alt="" />
      </div>
      <div className="col-8">
        <table class="table">
          <thead>
            <tr>
              <td colSpan="2">
                <h3>Thông số kỹ thuật</h3>
              </td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Màn hình</td>
              <td>{manHinh}</td>
            </tr>
            <tr>
              <td>Hệ điều hành</td>
              <td>{heDieuHanh}</td>
            </tr>
            <tr>
              <td>Camera trước</td>
              <td>{cameraTruoc}</td>
            </tr>
            <tr>
              <td>Camera sau</td>
              <td>{cameraSau}</td>
            </tr>
            <tr>
              <td>Ram</td>
              <td>{ram}</td>
            </tr>
            <tr>
              <td>Rom</td>
              <td>{rom}</td>
            </tr>
            <tr>
              <td>Giá</td>
              <td>{giaBan}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default memo(DetailProduct);
