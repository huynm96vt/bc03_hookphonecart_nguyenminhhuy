import React, { useCallback, useState } from "react";
import CardItem from "./CardItem";
import { mangSanPham } from "./data/dataPhone";
import DetailProduct from "./DetailProduct";
import ModalConfirm from "./ModalConfirm";

export default function HookPhoneCart() {
  const [data, setData] = useState(mangSanPham);
  const [show, setShow] = useState(false);
  const [detailProduct, setDetailProduct] = useState();
  const [gioHang, setGioHang] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);

  const handleOk = useCallback(() => {
    setIsModalVisible(false);
  }, []);
  const handleCancel = useCallback(() => {
    setIsModalVisible(false);
  }, []);
  const showModal = useCallback(() => {
    setIsModalVisible(true);
  }, []);

  let setProduct = useCallback((product) => {
    setDetailProduct(product);
  }, []);

  let handleShowDetail = useCallback(() => {
    setShow(true);
  }, []);

  let showDetail = (product) => {
    if (show) {
      return (
        <DetailProduct
          manHinh={product.manHinh}
          heDieuHanh={product.heDieuHanh}
          cameraTruoc={product.cameraTruoc}
          cameraSau={product.cameraSau}
          ram={product.ram}
          rom={product.rom}
          giaBan={product.giaBan}
          tenSP={product.tenSP}
          hinhAnh={product.hinhAnh}
        />
      );
    }
  };

  const themGioHang = useCallback(
    (product) => {
      let index = gioHang.findIndex((item) => {
        return item.maSP == product.maSP;
      });

      if (index == -1) {
        let newSP = { ...product, soLuong: 1 };
        let newGioHang = [...gioHang];
        newGioHang.push(newSP);
        setGioHang(newGioHang);
      } else {
        gioHang[index].soLuong++;
      }
    },
    [gioHang]
  );

  const tangGiamSoLuongSP = useCallback(
    (soluong, product) => {
      let index = gioHang.findIndex((item) => {
        return item.maSP == product.maSP;
      });
      let newGioHang = [...gioHang];
      newGioHang[index].soLuong += soluong;
      product.soLuong == 0 && newGioHang.splice(index, 1);
      setGioHang(newGioHang);
      if (gioHang.length === 0) {
        setIsModalVisible(false);
      }
    },
    [gioHang]
  );

  const xoaSP = useCallback(
    (product) => {
      let index = gioHang.findIndex((item) => {
        return item.maSP == product.maSP;
      });
      let newGioHang = [...gioHang];
      newGioHang.splice(index, 1);
      setGioHang(newGioHang);
    },
    [gioHang]
  );

  return (
    <div className="container">
      <div className="text-center pb-5">
        <h1>BT Gio Hang</h1>
      </div>
      <ModalConfirm
        isModalVisible={isModalVisible}
        handleOk={handleOk}
        handleCancel={handleCancel}
        gioHang={gioHang}
        tangGiamSoLuongSP={tangGiamSoLuongSP}
        xoaSP={xoaSP}
      />

      <div className="row">
        {data.map((item) => {
          return (
            <CardItem
              hinhAnh={item.hinhAnh}
              tenSP={item.tenSP}
              handleShowDetail={handleShowDetail}
              showDetail={showDetail}
              product={item}
              setProduct={setProduct}
              showModal={showModal}
              themGioHang={themGioHang}
            />
          );
        })}
      </div>
      {showDetail(detailProduct)}
    </div>
  );
}
