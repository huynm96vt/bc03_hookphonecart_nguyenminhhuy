import { Modal } from "antd";
import React, { memo } from "react";

function ModalConfirm({
  isModalVisible,
  handleOk,
  handleCancel,
  gioHang,
  tangGiamSoLuongSP,
  xoaSP,
}) {
  return (
    <Modal
      title="Gio hang"
      centered
      width={1000}
      visible={isModalVisible}
      onOk={handleOk}
      onCancel={handleCancel}
    >
      <table class="table">
        <thead></thead>
        <tbody>
          <tr>
            <td>Mã sản phẩm</td>
            <td>Hình ảnh</td>
            <td>Tên sản phẩm</td>
            <td>Số lượng</td>
            <td>Đơn giá</td>
            <td>Thành tiền</td>
            <td></td>
          </tr>

          {gioHang.map((item) => {
            return (
              <tr>
                <td>{item.maSP}</td>
                <td>
                  <img style={{ width: "50px" }} src={item.hinhAnh} alt="" />
                </td>
                <td>{item.tenSP}</td>
                <td>
                  <button
                    onClick={() => {
                      tangGiamSoLuongSP(1, item);
                    }}
                    style={{ width: "20px", padding: "0" }}
                    className="btn btn-primary container"
                  >
                    +
                  </button>
                  <span className="px-1">{item.soLuong}</span>
                  <button
                    onClick={() => {
                      tangGiamSoLuongSP(-1, item);
                    }}
                    style={{ width: "20px", padding: "0" }}
                    className="btn btn-primary"
                  >
                    -
                  </button>
                </td>
                <td>{item.giaBan}</td>
                <td>{item.soLuong * item.giaBan}</td>
                <td>
                  <button
                    onClick={() => {
                      xoaSP(item);
                    }}
                    className="btn btn-danger"
                  >
                    Xoá
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </Modal>
  );
}

export default memo(ModalConfirm);
